FROM node
MAINTAINER Juan Luis Del Valle Sotelo
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
CMD npm start
